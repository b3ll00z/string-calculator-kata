package stringcalculator;

import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.failBecauseExceptionWasNotThrown;

public class StringCalculatorTest {

    private StringCalculator stringCalculator;

    @Before
    public void setUp() {
        stringCalculator = new StringCalculator();
    }

    @Test
    public void testEmpty() {
        assertThat(stringCalculator.add("")).isEqualTo(0);
    }

    @Test
    public void testSumOneAndTwo() {
        assertThat(stringCalculator.add("1")).isEqualTo(1);
        assertThat(stringCalculator.add("2")).isEqualTo(2);
        assertThat(stringCalculator.add("1,2")).isEqualTo(3);
    }

    @Test
    public void testUnknownAmountOfNumbers() {
        assertThat(stringCalculator.add("10,1,1,23")).isEqualTo(35);
        assertThat(stringCalculator.add("12,9,2,1,5,0,3,4,8,24")).isEqualTo(68);
        assertThat(stringCalculator.add("111,2,11,20,4,9,17")).isEqualTo(174);
    }

    @Test
    public void testNewLinesBetweenNumbers() {
        assertThat(stringCalculator.add("1\n2,3")).isEqualTo(6);
    }

    @Test
    public void testDifferentSeparators() {
        assertThat(stringCalculator.add("//;\n1;2")).isEqualTo(3);
        assertThat(stringCalculator.add("//#\n,\n1,2#\n1,2")).isEqualTo(6);
    }

    @Test
    public void testIllegalArgumentExceptionOnNegativeNumber() {
        try {
            stringCalculator.add("-5");
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage("Negatives not allowed: -5");
        }
    }

    @Test
    public void testIllegalArgumentExceptionOnMultipleNegativeNumbers() {
        try {
            stringCalculator.add(",\n-1,11,-3,2,-5,\n1,2");
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage("Negatives not allowed: -1, -3, -5");
        }
    }

    @Test
    public void testAddInvocationCounts() {
        stringCalculator.add("//;\n1;2");
        stringCalculator.add("10,1,1,23");
        stringCalculator.add("2");
        assertThat(stringCalculator.getCalledCount()).isEqualTo(3);
    }

    @Test
    public void testIgnoreThousandNumbers() {
        assertThat(stringCalculator.add("2,1000")).isEqualTo(2);
        assertThat(stringCalculator.add("10,1001,1,23")).isEqualTo(34);
    }

    @Test
    public void testSeparatorOfAnyLength() {
        assertThat(stringCalculator.add("//[###]\n1###2###3")).isEqualTo(6);
    }

    @Test
    public void testMultipleSeparators() {
        assertThat(stringCalculator.add("//[#][%]\n1#2%3")).isEqualTo(6);
    }

    @Test
    public void testMultipleSeparatorsWithLengthLongerThanOne() {
        assertThat(stringCalculator.add("//[##][%%%]\n1##2,%%%3")).isEqualTo(6);
    }
}
