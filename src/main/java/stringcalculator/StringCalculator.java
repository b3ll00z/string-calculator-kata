package stringcalculator;

import java.util.ArrayList;
import java.util.List;

public class StringCalculator {

    private String separators = "\n|,";
    private Integer calledCount = 0;
    private List<String> negatives = new ArrayList<>();

    public int add(String expression) {
        incrementAddInvocationCounts();

        expression = extractOtherSeparators(expression);
        String[] operands = expression.split(separators);

        int sum = 0;

        for (String op : operands) {
            try {
                sum = includeNumber(sum, Integer.parseInt(op));
            } catch (NumberFormatException e) {
                sum += 0;
            }
        }

        checkNegativesOnExpression();

        return sum;
    }

    private int includeNumber(int sum, int number) {
        if (number < 0) {
            negatives.add(String.valueOf(number));
        }

        if (number < 1000)
            sum += number;

        return sum;
    }

    private void checkNegativesOnExpression() {
        if (!negatives.isEmpty())
            throw new IllegalArgumentException("Negatives not allowed: " + String.join(", ", negatives));
    }

    private void incrementAddInvocationCounts() {
        calledCount += 1;
    }

    private String extractOtherSeparators(String expression) {
        if (expression.startsWith("//")) {
            int startPositionOfSeparator = 2;
            int endPositionOfSeparator = expression.indexOf("\n");

            String declaredSeparators = expression.substring(startPositionOfSeparator, endPositionOfSeparator);
            includeDeclaredSeparators(declaredSeparators);

            String separator = expression.substring(startPositionOfSeparator, endPositionOfSeparator);
            includeNewSeparator(separator);

            expression = expression.substring(endPositionOfSeparator);
        }

        return expression;
    }

    private void includeDeclaredSeparators(String separatorDeclaration) {
        while (separatorDeclaration.startsWith("[") && separatorDeclaration.endsWith("]")) {
            int startOfSeparator = separatorDeclaration.indexOf("[") + 1;
            int endOfSeparator = separatorDeclaration.indexOf("]");

            String separator = separatorDeclaration.substring(startOfSeparator, endOfSeparator);
            includeNewSeparator(separator);

            separatorDeclaration = separatorDeclaration.substring(endOfSeparator + 1);
        }
    }

    private void includeNewSeparator(String separator) {
        separators += "|" + separator;
    }

    public int getCalledCount() {
        return calledCount;
    }
}
