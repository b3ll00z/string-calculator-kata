# String Calculator Kata

This exercise comes from [Roy Osherove](https://osherove.com/tdd-kata-1) kata.

The idea is to specify each of the kata requirements with the TDD principles,
based on test-first, coding and refactoring.